# LFS-ES

Traducción de LFS al español.
Si quieres contribuir, no lo dudes: entra en la rama _ingles_

LEER antes de corregir:
Las traducciones se harán siguiendo la guía de estilo de Debian
https://www.debian.org/international/spanish/notas
    
Se utilizará este glosario como referencia:
https://es.l10n.kde.org/glosario.php

    
Situación actual:

- SysV: Hecha la traducción pero falta gente que nos ayude a revisar las erratas.
- Systemd: Revisando las diferencias con SysV y modificando en la rama SystemD

Versión en inglés:
Copyright © 1999-2021 Gerard Beekmans
